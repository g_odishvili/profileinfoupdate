package com.android.tbctask6

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.android.tbctask6.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private var user: User? = User()


    companion object {
        const val USER_REQUEST_CODE = 1
        const val FIRST_NAME = "First Name: "
        const val LAST_NAME = "Last Name: "
        const val EMAIL = "Email: "
        const val GENDER = "Gender: "
        const val DATE_OF_BIRTH = "Date Of Birth: "
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()

        binding.btnUpdate.setOnClickListener {
            updateUser()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun init() {
        binding.tvFirstName.text = FIRST_NAME + user!!.firstName
        binding.tvLastName.text = LAST_NAME + user!!.lastName
        binding.tvEmail.text = EMAIL + user!!.email
        binding.tvBirthDate.text = DATE_OF_BIRTH + user!!.birthDate
        binding.tvGender.text = GENDER + user!!.gender
    }

    private fun updateUser() {
        val intent = Intent(this, EditProfile::class.java)
        intent.putExtra("user", user)
        startActivityForResult(intent, USER_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == USER_REQUEST_CODE && resultCode == RESULT_OK) {
            user = data?.getParcelableExtra("updatedUser")
            intent.extras?.remove("updatedUser")
            init()
        }
    }
}