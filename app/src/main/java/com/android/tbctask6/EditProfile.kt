package com.android.tbctask6

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.widget.EditText
import android.widget.RadioButton
import androidx.appcompat.app.AppCompatActivity
import com.android.tbctask6.databinding.ActivityEditProfileBinding
import java.util.*

class EditProfile : AppCompatActivity() {
    private lateinit var picker: DatePickerDialog
    private lateinit var eText: EditText
    private lateinit var userFromIntent: User
    private lateinit var gender: String

    private lateinit var binding: ActivityEditProfileBinding


    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEditProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()


        eText.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus)
                showDatePicker()
        }

        eText.setOnClickListener {
            showDatePicker()
        }

        binding.btnSave.setOnClickListener {
            sendUpdatedUser()
        }


    }

    private fun init() {
        eText = binding.edBirthDate

        userFromIntent = intent.getParcelableExtra("user")!!
        gender = userFromIntent.gender

        intent.extras?.remove("user")
        binding.edFirstName.setText(userFromIntent.firstName)
        binding.edLastName.setText(userFromIntent.lastName)
        binding.edEmail.setText(userFromIntent.email)
        binding.edBirthDate.setText(userFromIntent.birthDate)
        binding.edGender.check(if (userFromIntent.gender == "Male") R.id.rb_male else R.id.rb_female)
    }

    @SuppressLint("SetTextI18n")
    private fun showDatePicker() {
        eText.inputType = InputType.TYPE_NULL
        val cldr: Calendar = Calendar.getInstance()
        val day: Int = cldr.get(Calendar.DAY_OF_MONTH)
        val month: Int = cldr.get(Calendar.MONTH)
        val year: Int = cldr.get(Calendar.YEAR)
        // date picker dialog
        picker = DatePickerDialog(
            this@EditProfile,
            { _, year, monthOfYear, dayOfMonth -> eText.setText(dayOfMonth.toString() + "/" + (monthOfYear + 1) + "/" + year) },
            year,
            month,
            day
        )
        picker.show()
    }

    private fun sendUpdatedUser() {
        setData()

        val i = Intent()
        i.putExtra("updatedUser", userFromIntent)
        setResult(RESULT_OK, i)
        finish()
    }

    @SuppressLint("SetTextI18n")
    private fun setData() {
        userFromIntent.email = binding.edEmail.text.toString().trim()
        userFromIntent.firstName = binding.edFirstName.text.toString().trim()
        userFromIntent.lastName = binding.edLastName.text.toString().trim()
        userFromIntent.gender = gender
        userFromIntent.birthDate = binding.edBirthDate.text.toString().trim()
    }

    fun onRadioButtonClicked(view: View) {
        if (view is RadioButton) {
            // Is the button now checked?
            val checked = view.isChecked

            // Check which radio button was clicked
            when (view.getId()) {
                R.id.rb_male ->
                    if (checked) {
                        gender = "Male"
                    }
                R.id.rb_female ->
                    if (checked) {
                        gender = "Female"

                    }
            }
        }
    }
}