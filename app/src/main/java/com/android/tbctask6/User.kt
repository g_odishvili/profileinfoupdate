package com.android.tbctask6

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


@Parcelize
data class User(
    var firstName: String = "Giorgi",
    var lastName: String = "Odishvili",
    var email: String = "odishvili.giorgi@gmail.com",
    var birthDate: String = "30/06/2001",
    var gender: String = "Male"
) : Parcelable
